<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return "Index dari post";
    }

    public function show($id)
    {
        return "Ini id nya: $id";
    }

    public function create()
    {
        return view('create');
    }

    public function handlePost(Request $request) //object baru namanya $request yang merupakan hasil pendeklarasian object baru menggunakan kelas request
    {
        // dd($request->all());
        $namaLengkap = $request["namaLengkap"];
        $usia = $request["usia"];

        return "Nama Lengkap : $namaLengkap";
    }
}
