<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); //di resources, views, welcomebladephp
});

Route::get('/test', function () {
    $nama = "Vigi";
    return view('test', compact('nama')); //compact mengangkut data2 yang ingin ditampilkan di php
});

Route::get('/hitung/{angka1}/tambah/{angka2}', function ($angka1, $angka2) {
    $hasil = $angka1 + $angka2;
    return $hasil;
});

Route::get('/sapa/{nama}', function ($nama) {
    return "Halo $nama, Selamat Datang!";
});

Route::get('/post', 'PostController@index'); //diarahkan ke file yang namanya PostController di function index

Route::get('/post/create', 'PostController@create');
Route::get('/post/{id}', 'PostController@show');

Route::post('/newpost', 'PostController@handlePost');
